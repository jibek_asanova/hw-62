import './App.css';
import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import AboutMe from "./Container/AboutMe/AboutMe";
import Navbar from "./Components/Navbar/Navbar";
import Footer from "./Components/Footer/Footer";
import Portfolio from "./Container/Portfolio/Portfolio";
import Header from "./Container/Header/Header";

function App() {
  return (
      <BrowserRouter>
          <Navbar/>
          <Switch>
              <Route path="/" exact component={Header}/>
              <Route path="/about" component={AboutMe}/>
              <Route path="/portfolio" component={Portfolio}/>
          </Switch>
          <Footer/>
      </BrowserRouter>

  );
}

export default App;
