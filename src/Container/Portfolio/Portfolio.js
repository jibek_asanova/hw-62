import React from 'react';
import './Portfolio.css';
import cartPic1 from '../../accets/pic-1.jpg';
import cartPic2 from '../../accets/pic-2.jpg';
import cartPic3 from '../../accets/pic-3.jpg';
import cartPic4 from '../../accets/pic-4.jpg';

const Portfolio = () => {
    return (
        <div className="cards-block container mb-5">
            <h2 className="text-uppercase mb-5">Recent Work Done on <span className="text-success">Job Board</span></h2>

            <div className="row">
                <div className="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div className="card text-center text-uppercase mb-5">
                        <img className="card-img-top" src={cartPic1} alt="Card cap"/>
                            <div className="card-body">
                                <h5 className="card-title  mt-3">Round Icons Design</h5>
                            </div>
                    </div>
                </div>
                <div className="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div className="card text-center text-uppercase mb-5">
                        <img className="card-img-top" src={cartPic2} alt="Card cap"/>
                            <div className="card-body">
                                <h5 className="card-title  mt-3">Stationary Design</h5>
                            </div>
                    </div>
                </div>
                <div className="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div className="card text-center text-uppercase mb-5">
                        <img className="card-img-top" src={cartPic3} alt="Card cap"/>
                            <div className="card-body">
                                <h5 className="card-title  mt-3">Logo Design</h5>
                            </div>
                    </div>
                </div>
                <div className="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div className="card text-center text-uppercase mb-5">
                        <img className="card-img-top" src={cartPic4} alt="Card cap"/>
                            <div className="card-body">
                                <h5 className="card-title  mt-3">Branding</h5>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Portfolio;