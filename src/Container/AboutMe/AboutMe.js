import React from 'react';
import './AboutMe.css';
import pic1 from '../../accets/pic-big-1.png';
import pic2 from '../../accets/pic-big-2.png';

const AboutMe = () => {
    return (
        <div>
            <div className="services-block text-center py-5">
                <div className="container">
                    <h2 className="text-success text-uppercase"><span className="text-dark">We give</span> best Services
                    </h2>
                    <p className="mx-auto mb-5">At vero eos et accusamus et iusto odio dignissimos ducimus qui
                        blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias
                        excepturi sint occaecati cupiditate non providen</p>
                    <div className="row">
                        <div className="col-12 col-sm-6 col-lg-3">
                            <div className="icon icon-1 mb-4"> </div>
                            <h4 className="text-success text-uppercase mb-4">New Way Of Vision</h4>
                            <p>Temporibus autem quibusdam et aut
                                officiis debitis aut rerum necessitatibus
                                saepe eveniet ut et voluptates</p>
                        </div>
                        <div className="col-12 col-sm-6 col-lg-3">
                            <div className="icon icon-2 mb-4"> </div>
                            <h4 className="text-success text-uppercase mb-4">Fresh Ideas</h4>
                            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet
                                ut et voluptates</p>
                        </div>
                        <div className="col-12 col-sm-6 col-lg-3">
                            <div className="icon icon-3 mb-4"> </div>
                            <h4 className="text-success text-uppercase mb-4">Strong Foundations</h4>
                            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet
                                ut et voluptates</p>
                        </div>
                        <div className="col-12 col-sm-6 col-lg-3">
                            <div className="icon icon-4 mb-4"> </div>
                            <h4 className="text-success text-uppercase mb-4">Great Support</h4>
                            <p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet
                                ut et voluptates</p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="text-block-1 container">
                <div className="row align-items-center">
                    <div className="col-12 col-lg-6 my-5 text-block pic-1">
                        <img src={pic1} alt="pic-big-1"/>
                    </div>
                    <div className="col-12 col-lg-6 text-block-text">
                        <h2 className="text-uppercase">Find <span className="text-success">the perfect</span> match fast
                        </h2>
                        <p>Start your job in hours, not weeks. Get a shortlist of skilled freelancers instantly, tapping
                            into our hiring know-how and matching technology. Interview favorites online and hire with
                            the click of a button. </p>
                    </div>

                </div>
            </div>

            <div className="text-block-2 mb-5">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-12 col-lg-6 text-block-text">
                            <h2 className="text-uppercase mt-5 mb-4">Great <span
                                className="text-success">work starts</span> with great talent</h2>

                            <ul className="work-list">
                                <li>Get amazing results working with the best programmers</li>
                                <li>Designers, writers and other top online pros</li>
                                <li>Hire freelancers with confidence, always knowing</li>
                                <li>Their work experience and feedback from other clients</li>
                                <li>Designers, writers and other top online pros .</li>
                            </ul>
                        </div>
                        <div className="col-12 col-lg-6 my-5 text-block pic-2">
                            <img src={pic2} alt="pic-big-2"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutMe;