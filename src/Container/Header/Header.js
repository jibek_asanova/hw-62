import React from 'react';
import './Header.css';
import background from '../../accets/main-pic.jpg';

const Header = () => {
    return (
        <div className="main-block text-center" style={{ backgroundImage: `url(${background})` }}>
            <div className="container">
                <h1 className="text-success text-uppercase mx-auto">Welcome<span
                    className="text-dark"> to my </span>page!</h1>
            </div>
        </div>
    );
};

export default Header;