import React from 'react';
import logo from '../../accets/logo.png';
import './Navbar.css';
import {NavLink} from "react-router-dom";


const Navbar = () => {
    return (
        <div className="nav-bg">
            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-light ">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">
                            <img src={logo} alt="Joby job finder"/>
                        </a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"> </span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <NavLink exact to="/" className="nav-link" >Home</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/about" className="nav-link" >About</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/portfolio" className="nav-link" >Portfolio</NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

    );
};

export default Navbar;